//
//  PBC_CocktailApp.swift
//  PBC Cocktail
//
//  Created by Megan Ehrlich on 2/1/22.
//

import SwiftUI

@main
struct PBC_CocktailApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}

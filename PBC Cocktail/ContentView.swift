//
//  ContentView.swift
//  PBC Cocktail
//
//  Created by Megan Ehrlich on 2/1/22.
//

import SwiftUI

struct ContentView: View {
    
    
    let whisky = ["Old Fashioned", "Manhattan", "Hanne-Hanne", "Sazerac", "Vieux Carre", "Raincoat"]
    let gin = ["Martini", "Alaska", "Aviation", "20th Century", "Martinez", "Saturn", "Symphony", "Vesper"]
    let tequila = ["Margarita", "El Diablo", "Sandy The Showgirl"]
    let rum = ["El Presidente", "El Draque", "Daiquiri"]
    let dessert = ["Barbary Coast", "Brandy Alexander", "Colleen Bawn", "Golden Cadillac", "Grasshopper", "Jamaican Milk Punch", "Mai Tai", "Pink Squirrel", "Pisco Punch", "Singapore Sling", "Stinger", "White Nun", "Irish Coffee"]
    @State var whiskyList = [Int]()
    @State var ginList = [Int]()
    @State var tequilaList = [Int]()
    @State var rumList = [Int]()
    @State var dessertList = [Int]()
    @State var message = " "
    @State var ingredients = " "
    
    var body: some View {
        
        ZStack {
            Image("pelican").resizable().ignoresSafeArea()
            
            VStack{
                
                Spacer()
                Text("Megan's Cocktail Generator")
                    .font(.title2)
                    .foregroundColor(.white)
                Text("Pelican Boat Club Edition").foregroundColor(.white).padding()
                Spacer()
                Text(message).font(.title).foregroundColor(.white)
                
                Text(ingredients).font(.body).foregroundColor(.white)
                Spacer()
                Group {
                    Button("Gin") {
                        
                        let randNumber = Int.random(in:0...gin.count-1)
                        let generatedGin = gin[randNumber]
                        
                        message = generatedGin
                        
                        if randNumber == 0 {
                            ingredients = "Gin, vermouth"
                        }//if
                        
                        else if randNumber == 1 {
                            ingredients = "Gin, yellow chartreuse, bitters"
                        }//else if
                        
                        else if randNumber == 2 {
                            ingredients = "Gin, creme de violette, maraschino, lemon"
                        }//else if
                        
                        else if randNumber == 3 {
                            ingredients = "Gin, lillet blanc, creme de cacao, lemon"
                        }//else if
                        
                        else if randNumber == 4 {
                            ingredients = "Gin, , maraschino, vermouth"
                        }//else if
                        
                        else if randNumber == 5 {
                            ingredients = "Gin, falarnum, orgeat, passion fruit, lemon"
                        }//else if
                        
                        else if randNumber == 6 {
                            ingredients = "Gin, Whisky, maraschino, chartreuse, lemon"
                        }//else if
                        
                        else if randNumber == 7 {
                            ingredients = "Gin, Vodka, lillet"
                        }//else if
                                    
                        
                    }//Button
                    
                    Button("Rum") {
                        
                        let randNumber = Int.random(in:0...rum.count-1)
                        let generatedRum = rum[randNumber]
                        
                        message = generatedRum
                        
                        if randNumber == 0 {
                            ingredients = "Rum, curacao, vermouth, grenadine"
                        }
                        else if randNumber == 1 {
                            ingredients = "Cachaca, sugar, lime, mint"
                        } //else if
                        else if randNumber == 2 {
                            ingredients = "Rum, sugar, lime"
                        }

                        
                    }//Button
                    
                    Button("Tequila") {
                        
                        let randNumber = Int.random(in:0...tequila.count-1)
                        let generatedTequila = tequila[randNumber]
                        
                        message = generatedTequila
                        
                        if randNumber == 0 {
                            ingredients = "Tequila, triple sec, lime"
                        }//if
                        
                        else if randNumber == 1 {
                            ingredients = "Tequila, ginger, creme de cassis"
                        }//else if
                        
                        else if randNumber == 2 {
                            ingredients = "Tequila, mezcal, maraschino"
                        }//else if
                    }//Button
                    
                    Button("Whisky") {
                        
                        let randNumber = Int.random(in:0...whisky.count-1)
                        let generatedWhisky = whisky[randNumber]
                        
                        message = generatedWhisky
                        
                        if randNumber == 0 {
                            ingredients = "Whisky, simple syrup, bitters"
                        }//if
                        
                        else if randNumber == 1 {
                            ingredients = "Bourbon, sweet vermouth, bitters"
                        }//else if
                        
                        else if randNumber == 2 {
                            ingredients = "Scotch, China-China"
                        }//else if
                        
                        else if randNumber == 3 {
                            ingredients = "Rye, Absinthe, bitters"
                        }//else if
                        
                        else if randNumber == 4 {
                            ingredients = "Rye, Cognac, Benedictine, vermouth"
                        }//else if
                        
                        else if randNumber == 5 {
                            ingredients = "Bourbon, nocino, orgeat"
                        }//else if
                    
                    }//Button
                    
                    Button("Dessert") {
                        
                        let randNumber = Int.random(in:0...dessert.count-1)
                        let generatedDessert = dessert[randNumber]
                        
                        message = generatedDessert
                        
                        if randNumber == 0 {
                            ingredients = "Whisky, gin, creme de cacao, cream"
                        }//if
                        
                        else if randNumber == 1 {
                            ingredients = "Cognac, creme de cacao, cream"
                        }//else if
                        
                        else if randNumber == 2 {
                            ingredients = "Rye, yellow chartreuse, benedictine"
                        }//else if
                        
                        else if randNumber == 3 {
                            ingredients = "Galliano, creme de cacao, cream, orange"
                        }//else if
                        
                        else if randNumber == 4 {
                            ingredients = "Creme de menthe, creme de cacao, cream"
                        }//else if
                        
                        else if randNumber == 5 {
                            ingredients = "Jamaican rum, cream, milk, vanilla"
                        }//else if
                        
                        else if randNumber == 6 {
                            ingredients = "Rum, curacao, orgeat, lime, mint"
                        }//else if
                        
                        else if randNumber == 7 {
                            ingredients = "Creme de noyau, creme de cacao, cream"
                        }//else if
                        
                        else if randNumber == 8 {
                            ingredients = "Pisco, pineapple, lemon"
                        }//else if
                        
                        else if randNumber == 9 {
                            ingredients = "Gin, cherry heering, benedictine, cointreau, grenadine, pineapple, lime"
                        }//else if
                        
                        else if randNumber == 10 {
                            //else if
                            ingredients = "Cognac, creme de menthe"
                        }//else if
                        
                        else if randNumber == 11 {
                            ingredients = "Cognac, kahula, cream"
                        }
                        
                        else if randNumber == 12 {
                            ingredients = "Irish whisky, coffee, sugar, cream"
                        }
                    }//Button
                    
                    Button("Restart") {

                        whiskyList.removeAll()
                        tequilaList.removeAll()
                        rumList.removeAll()
                        ginList.removeAll()
                        dessertList.removeAll()
                        message = " "
                        ingredients = " "
                    }
                    .padding(.vertical)//Button Restart
                    
                }//Button Group
                .padding(.bottom)
            }//VStack
        }//ZStack
        
    }//Body
}//Content

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

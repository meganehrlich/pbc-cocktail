# Planning

* [] Re-implement code to only suggest each cocktail once
* [] Be more specific with search criteria
* [] Finish adding to apple store

## Overview

PBC cocktail app to randomly generate cocktails based on user input

